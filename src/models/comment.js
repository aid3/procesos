const bookshelf = require('../conf/bookshelf');

const AIDComment = bookshelf.model('Comment',{
    tableName: 'aid_comentario'
});

module.exports = AIDComment;
