const bookshelf = require('../conf/bookshelf');
const comments = require('./comment');

const AIDTicket = bookshelf.model('Ticket',{
    tableName: 'aid_ticket',
    reporter() {
        return this.belongsTo('Reporter','reporter_id','id')
    },
    asignee() {
        return this.belongsTo('User','asignee_id','id')
    },
    department() {
        return this.belongsTo('Department','department_id','id')
    },
    // comment() {
    //     return this.belongsToMany('Comment','aid_comentario','id')
    // }
    comments: function() {
        return this.hasMany(comments, 'ticket_id');
    }
});

module.exports = AIDTicket;
