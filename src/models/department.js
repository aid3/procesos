const bookshelf = require('../conf/bookshelf');

const AIDDepartment = bookshelf.model('Department',{
    tableName: 'aid_department',
    users() {
        return this.hasMany('User', "id", "department_id");
    }
});

module.exports = AIDDepartment;