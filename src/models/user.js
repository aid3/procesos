const bookshelf = require('../conf/bookshelf');

const AIDUser = bookshelf.model('User',{
    tableName: 'aid_user',
    department() {
        return this.belongsTo('Department','department_id','id')
    }
});

module.exports = AIDUser;