const bookshelf = require('../conf/bookshelf');

const AIDReporter = bookshelf.model('Reporter',{
    tableName: 'aid_reporter'
});

module.exports = AIDReporter;