/**
 * Constante con el token secreto para codificación.
 * @type {{TOKEN_SECRET: (string|string)}}
 */
module.exports = {
    TOKEN_SECRET: process.env.TOKEN_SECRET || "tokenultrasecreto"
};
