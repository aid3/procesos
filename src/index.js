const express = require('express');
const app = express();
const cors = require("cors");
const morgan = require('morgan');
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')

// setingss de express
app.set('port', process.env.PORT || 3000);
app.use(cors({ origin: "*" }));

//Middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());



//Rutas principales
app.use('/tickets',require('./routes/ticketsRoutes'));
app.use('/departments',require('./routes/departmentsRoutes'));
app.use('/reporters',require('./routes/reportersRoutes'));
app.use('/users',require('./routes/usersRoutes'));
app.use('/tokens',require('./routes/tokensRoutes'));
app.use('/login',require('./routes/login'));
app.use('/comment',require('./routes/commentsRoutes'));

//Swagger, documentar las apis.
const swaggerOptions = {  
  swaggerDefinition:{
    info:{
      title: 'Api AID',
      description: "API para el proyecto AID",
      contact:{
        name: "Douglas Aguilar"
      },
      servers: ["0.0.0.0"]
    }
  },
  apis: ['./src/routes/*.js']
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));


// Iniciar servidor
let server = app.listen(app.get('port'), () => {
  console.log('Aplicacion en puerto: ', app.get('port'))
});

module.exports = server;
