/**
 * Clase inicial, que presenta la interfaz de respuesta del sistema.
 * @type {Result}
 */
let Result = class Result  {
    constructor() {
        this.status = false;
        this.message = 'Error al procesar solicitud.';
    }

    get getStatus() {
        return status;
    }

    get getMessage() {
        return message;
    }

    set setStatus(status) {
        this.status = status;
    }

    set setMessage(message) {
        this.message = message;
    }
}

let NokResult = class NokResult extends Result {
    constructor() {
        super();
        this.exception = [];
    }

    get getException() {
        return exception;
    }

    set setException(exception) {
        this.exception = exception;
    }
}

let OkResult = class OkResult extends Result {
    constructor() {
        super();
        this.status = true;
        this.message = 'Se procesa solicitud correctamente.';
        this.result = [];
    }

    get getResult() {
        return result;
    }

    set setResult(result) {
        this.result = result;
    }
}

module.exports.Result = Result;
module.exports.NokResult = NokResult;
module.exports.OkResult = OkResult;

