const express = require('express');
const router = express.Router();

const reportersController = require('../controllers/reporters');

router.post('/',reportersController.create);

router.put('/',reportersController.update);

router.get('/:id',reportersController.get);

router.get('/',reportersController.getAll);

router.delete('/:id',function (req, res, next) {
    next(new Error('not implemented'))
});

module.exports = router;