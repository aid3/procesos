const express = require('express');
const router = express.Router();

const departmentsController = require('../controllers/departments');

router.post('/',departmentsController.create);

router.put('/',departmentsController.update);

router.get('/:id',departmentsController.get);

router.get('/',departmentsController.getAll);

router.delete('/:id',function (req, res, next) {
    next(new Error('not implemented'))
});

module.exports = router;