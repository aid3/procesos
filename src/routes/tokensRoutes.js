const express = require('express');
const router = express.Router();

const tokensController = require('../controllers/token');

router.post('/',tokensController.validateToken);

module.exports = router;
