const express = require('express');
const router = express.Router();
const commentsController = require('../controllers/comments');

router.post('/',commentsController.create);

router.put('/',commentsController.update);

router.get('/',function (req, res, next) {
    next(new Error('not implemented'))
});

router.get('/:id',function (req, res, next) {
    next(new Error('not implemented'))
});

router.delete('/:id',commentsController.del);


module.exports = router;
