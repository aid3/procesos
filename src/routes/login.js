const express = require('express');
const router = express.Router();
const loginController = require('../controllers/login');
/**
 * @swagger
 * /:
 *  get:
 *      description: Usada para probar si api esta corriendo
 *      responses:
 *          '200':
 *              description: Respuesta correcta
 *              
 */
router.post('/',loginController.login)

module.exports = router;
