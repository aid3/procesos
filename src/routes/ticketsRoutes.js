const express = require('express');
const router = express.Router();

const ticketsController = require('../controllers/tickets');

router.post('/',ticketsController.create);

router.put('/',ticketsController.update);

router.get('/:id',ticketsController.get);

router.get('/',ticketsController.getAll);

router.delete('/:id',function (req, res, next) {
    next(new Error('not implemented'))
});

module.exports = router;