const express = require('express');
const router = express.Router();

const usersController = require('../controllers/users');

router.post('/',usersController.create);

router.put('/',usersController.update);

router.get('/:id',usersController.get);

router.get('/',usersController.getAll);

router.delete('/:id',function (req, res, next) {
    next(new Error('not implemented'))
});

module.exports = router;