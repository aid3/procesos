var jwt = require('jwt-simple')
var moment = require('moment')
var config = require('../conf/configToken')
const AIDReporters = require('../models/reports');
const Result = require('../interfaces/result');

/**
 * Creare token de seguridad, este sirve para las sesiones.
 * @param user
 * @returns {string}
 */
const createSecurityToken = user => {
    var playload = {
        sub: user,
        iat: moment().unix,
        exp: moment().add(2, "hours").unix()
    };
    return jwt.encode(playload, config.TOKEN_SECRET);
}

/**
 * Crea un token que se enviara al usuario para que lo ingrese de forma manual.
 * @param user
 * @returns {string}
 */
const createExternalToken = user => {
    var playload = {
        //sub: user.id,
        //iat: moment().unix,
        exp: moment().add(10, "hours").unix()
    };
    return jwt.encode(playload, config.TOKEN_SECRET);
}

/**
 * Con este metodo validamos que el token que el usuario ingresa de manera manual sea correcto.
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function validateToken(req,res) {
    const token = req.body.token;

    console.log(token);
    try{
        var user = await AIDReporters.where("id", token).fetch({require: false});

    }catch (e) {
        console.error(e)
        res.status(200).send(new Result.NokResult());
        return;
    }

    if (!user || user.id===undefined ) { res.status(200).send({'valid': false, 'user': null}); return;}

    user = user.toJSON();
    console.log(user);
    console.log(user.used);

    if (user.used===1) { res.status(200).send({'valid': false, 'user': null}); return;}

    res.json({'valid': true, 'user': user});
}


module.exports.createSecurityToken = createSecurityToken;
module.exports.createExternalToken = createExternalToken;
module.exports.validateToken = validateToken;
