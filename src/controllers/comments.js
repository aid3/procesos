const AIDComment = require('../models/comment');
const Result = require('../interfaces/result')
// const OkResult = require('../interfaces/OkResult')

/**
 * Crear
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function create(req,res){
    console.log("createComment");

    const dateManipulator = new ControlFechas();
    const today = dateManipulator.getFormatedDate();
    console.log(today);

    //creamos la clase
    try{
        var comment = await AIDComment.forge({
            contenido: req.body.contenido,
            user_id: req.body.user_id,
            ticket_id: req.body.ticket_id,
            comment_date: today
        }).save();
    }catch (e) {
        console.error(e);
        var response = new Result.NokResult();
        response.message = 'Error al crear el comentario.';
        res.status(200).send(response);
        return ;
    }

    let OkResponse = new Result.OkResult();
    OkResponse.result = comment;
    res.json(OkResponse);
}

/**
 * Actualizar comentario
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function update(req,res){
    console.log("updateComment:");
    try{
        var comment = await AIDComment.where("id", req.body.id).save(
            { contenido: req.body.contenido },
            { patch: true }
        );
    }catch (e) {
        console.error(e);
        var response = new Result.NokResult();
        response.message = 'Error al editar el comentario.';
        res.status(200).send(response);
    }

    let OkResponse = new Result.OkResult();
    OkResponse.result = comment;
    res.json(OkResponse);
}

/**
 * Eliminar comentario.
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function del(req,res){
    console.log("deleteComment:");
    try{
        var comment = await AIDComment.where('id', req.params.id).destroy();
    }catch (e) {
        console.error(e)
        var response = new Result.NokResult();
        response.message = 'Error al eliminar el comentario.';
        res.status(200).send(response);
    }

    let OkResponse = new Result.OkResult();
    OkResponse.result = comment;
    res.json(OkResponse);
}

/**
 * Clase que sirve para manipulación de fechas.
 */
class ControlFechas {
    constructor() {
        this.timeElapsed = Date.now();
        this.today = new Date(this.timeElapsed);
    }

    get getTimeElapsed() {
        return timeElapsed;
    }

    set setTimeElapsed(timeElapsed) {
        this.timeElapsed = timeElapsed;
    }

    get getToday() {
        return today;
    }

    set setToday(today) {
        this.timeElapsed = today;
    }

    getFormatedDate(){
        // const timeElapsed = Date.now();
        // let today = new Date(this.timeElapsed);
        //today = today.toISOString();

        let day = this.today .getDate();
        let month = this.today .getMonth()+ 1;
        let year = this.today .getFullYear();
        let hour = this.today .getHours();
        let min = this.today .getMinutes();
        let sec = this.today .getSeconds();

        const result = year  + "-" + month  + "-" + day + " " +hour + ":" +min + ":" +sec;

        return result;
    }


}


module.exports = {
    create,
    update,
    del
}
