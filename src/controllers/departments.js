const AIDDepartment = require('../models/department');

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function create(req,res){
    console.log("createDepartment");

    var department = await AIDDepartment.forge({
            name: req.body.name,
            description: req.body.description
        }).save();
    res.json(department);
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function get(req,res){
    console.log("getDepartment:"+req.params.id);
    var department = await AIDDepartment.where("id", req.params.id).fetch();
    res.json(department);
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function getAll(req, res) {
    console.log("getAllDepartment");
    var departments = await new AIDDepartment().fetchAll();
    res.json(departments);
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function update(req,res){
    console.log("updateDepartment:");
    try{
        var department = await AIDDepartment.where("id", req.body.id).save(
            { ...req.body },
            { patch: true }
        );
    }catch (e) {
        console.error(e)
        res.status(200).send('Error en procesar datos');
    }

    res.json(department);
}

module.exports = {
    create,
    get,
    update,
    getAll
}
