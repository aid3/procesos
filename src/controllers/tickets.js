const AIDTicket = require('../models/ticket');
const AIDReporters = require('../models/reports');
const Result = require('../interfaces/result');

/**
 * Crear ticket.
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function create(req,res){
    console.log("createTicket");
    try{
        var ticket = await AIDTicket.forge({
            summary: req.body.summary,
            description: req.body.description,
            priority: req.body.priority,
            state: req.body.state,
            department_id: req.body.department_id,
            asignee_id: req.body.asignee_id,
            reporter_id: req.body.reporter_id
        }).save();

        var user = await AIDReporters.where("id", req.body.reporter_id).save(
            { 'used' : '1' },
            { patch: true }
        );
    }catch (e) {
        console.error(e)
        res.status(200).send('Error en procesar datos');
    }

    res.json(ticket);
}

/**
 * Obtener el detalle del ticket.
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function get(req,res){
    console.log("getTicket:"+req.params.id);

    try{
        var ticket = await AIDTicket.where("id", req.params.id).fetch({
            withRelated: ['department','reporter','asignee','comments'],
            require: false
        });

    }catch (e) {
        console.error(e)
        res.status(200).send('Error en procesar datos');
    }

    if (!ticket|| !ticket.id || ticket.id===undefined ) {
        var response = new Result.NokResult();
        response.message = 'El ticket no existe.';
        res.json(response);
        return;
    }

    res.json(ticket);
}

/**
 * Obtener el listado de tickets.
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function getAll(req, res) {
    console.log("getAllTickets");
    var departments = await new AIDTicket().fetchAll();
    res.json(departments);2
}


/**
 * Actualizar el ticket.
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function update(req,res){
    console.log("updateTicket");
    try{
        var department = await AIDTicket.where("id", req.body.id).save(
            { ...req.body },
            { patch: true }
        );
    }catch (e) {
        console.error(e)
        res.json('Error en procesar datos');
    }

    res.json(department);
}

module.exports = {
    create,
    get,
    update,
    getAll
}
