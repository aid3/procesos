const AIDUser = require('../models/user');
const bcrypt = require('bcryptjs');

const {createExternalToken} = require('./token');

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function create(req,res){
    console.log("createUser");

    //validate user by email
    var user = await AIDUser.where({email: req.body.email}).fetch({require: true}).catch(error => {
        console.log("Ususario nuevo.");
    });

    if (user|| user != undefined) { res.json({'status': false, 'message': 'Ya existe un usuario creado con este email.'}); return;}

    try{

        //hash password
        const salt = await bcrypt.genSalt(10);
        const hashpassword = await bcrypt.hash(req.body.password,salt);
        user = await AIDUser.forge({
            name: req.body.name,
            lastname: req.body.lastname,
            email: req.body.email,
            department_id: req.body.department_id,
            password: hashpassword
        }).save();
    }catch (e) {
        console.error(e)
        res.status(500).send('Error en procesar datos');
        return;
    }

    res.json({'status': true, 'message': 'Usuario creado exitosamente.', user: user});
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function get(req,res){
    console.log("getUser:"+req.params.id);
    var user = await AIDUser.where("id", req.params.id).fetch({
        withRelated: ['department']
    });
    res.json(user);
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function getAll(req, res) {
    console.log("getAllUsers");
    var departments = await new AIDUser().fetchAll();
    res.json(departments);
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function update(req,res){
    console.log("updateUser");
    try{
        var user = await AIDUser.where("id", req.body.id).save(
            {
                name: req.body.name,
                lastname: req.body.lastname,
                email: req.body.email,
                department_id: req.body.department_id
            },
            { patch: true }
        );
    }catch (e) {
        console.error(e)
        res.status(200).send('Error en procesar datos');
    }

    res.json(user);
}

module.exports = {
    create,
    get,
    update,
    getAll
}
