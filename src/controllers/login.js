const AIDUser = require('../models/user');
const bcrypt = require('bcryptjs');
const tokensController = require('../controllers/token');
const Result = require('../interfaces/result');

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function login(req, res) {
    console.log("login");

    var user = await AIDUser.where({email: req.body.email}).fetch({require: true}).catch(error => {
        console.error(error);
        var response = new Result.NokResult();
        response.message = 'El usuario no existe.';
        res.status(200).send(response);
        return;
    });

    user = user = user.toJSON();

    //el usuario no existe
    if (!user|| !user.password || user.password===undefined ) {
        var response = new Result.NokResult();
        response.message = 'El usuario no existe.';
        res.status(200).send(response);
        return;
    }

    //el password es el de base de datos, validamos
    var validPassword = await bcrypt.compare(req.body.password,user.password); //req.body.password;
    console.log(validPassword);

    //respondemos si no es valido
    if (!validPassword) {
        var response = new Result.NokResult();
        response.message = 'Error en el email-password.';
        res.status(200).send(response);
        return;
    };

    res.json({'status': validPassword, 'message': 'Login exitoso.', 'securityToken': tokensController.createSecurityToken(), result: user});
}

module.exports = {
    login
}
