const AIDReporters = require('../models/reports');
const Result = require('../interfaces/result');

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function create(req,res){
    console.log("createReporter");

    try{
        var user = await AIDReporters.forge({
            name: req.body.name,
            lastname: req.body.lastname,
            email: req.body.email,
        }).save();
    }catch (e) {
        console.error(e)
        res.status(200).send(new Result.NokResult());
        return;
    }

    res.json({user: user,token: user.id});
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function get(req,res){
    console.log("getReporter:"+req.query.id);
    var department = await AIDReporters.where("id", req.params.id).fetch();
    res.json(department);
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function getAll(req, res) {
    console.log("getAllReporter");
    var departments = await new AIDReporters().fetchAll();
    res.json(departments);
}

/**
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function update(req,res){
    console.log("updateReporter:");
    try{
        var department = await AIDReporters.where("id", req.body.id).save(
            { ...req.body },
            { patch: true }
        );
    }catch (e) {
        console.error(e)
        res.status(200).send('Error en procesar datos');
    }

    res.json(department);
}

module.exports = {
    create,
    get,
    update,
    getAll
}
