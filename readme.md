**1) Instalar Dependencias del proyecto:**
    - bookshelf y knex son el ORM para la conexion a la BD (https://bookshelfjs.org/index.html)
    - express: sirve para crear los servicios (http://expressjs.com/es/api.html#req)
    - swagger: documentar las apis (https://medium.com/@diegopm2000/creando-un-api-rest-con-swagger-node-c880bdac04a5)

**2) Crea una base de datos de mysql (mariadb)**
    - Cambia las credenciales en el archivo: /src/conf/bookshelf.js
    - Correr los scripts /scripts/aid_creat.sql

**3) Levantar el servidor, npm start**


**4) Usar los servicios, algunos ejemplos son (tienen metodos get,post y put, no delete, el get tiene dos versiones una sin el id en la url, para obtener el listado y otra con el id, esta para el detalle):**

```
http://localhost:3000/tickets/

http://localhost:3000/departments/

http://localhost:3000/users/

http://localhost:3000/reporters/
```



Si se desea probar los servicios, bajar postman e importar el archivo:  **AID.postman_collection.json**

