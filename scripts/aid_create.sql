
create table aid_department
(
    id smallint not null AUTO_INCREMENT
        primary key,
    name varchar(255) not null,
    description varchar(255) null
);

create table aid_user
(
    id smallint not null AUTO_INCREMENT
        primary key,
    name varchar(255) null,
    lastname varchar(255) null,
    email varchar(255) null,
    department_id smallint not null,
    constraint FK_USER_DEPARTMENT_
        foreign key (department_id) references aid_department (id)
);

create table aid_reporter
(
    id smallint not null AUTO_INCREMENT
        primary key,
    name varchar(255) null,
    lastname varchar(255) null,
    email varchar(255) not null,
    access_token varchar(255) not null
);


create table aid_ticket
(
    id smallint not null AUTO_INCREMENT
        primary key,
    summary varchar(255) null,
    description varchar(255) null,
    priority varchar(255) null,
    state varchar(255) null,
    reporter_id smallint not null,
    asignee_id smallint not null,
    department_id smallint not null,
    constraint fk_ticket_department_
        foreign key (department_id) references aid_department (id),
    constraint fk_ticket_reporter_
        foreign key (reporter_id) references aid_reporter (id),
    constraint fk_ticket_user_
        foreign key (asignee_id) references aid_user (id)
);



insert into aid_department (name, description) values ( 'Contabilidad', 'Departamento encargado de la contabilidad');
insert into aid_user (name, lastname, email, department_id) values ( 'Douglas', 'Aguilar', 'correo@gmail.com', 1);
insert into aid_reporter (name, lastname, email, access_token) values ( 'Douglas', 'Aguilar', 'correo@gmail.com', 'TOKEN01');
insert into aid_ticket (summary, description, priority, state, reporter_id, asignee_id, department_id) values ( 'Resumen tiket 01', 'Descripcion tiket 01', 'media', 'finalizado', 1,1,1);

/*    04/10    */

ALTER TABLE aid_reporter
    ADD COLUMN used boolean default false;


/*  13/10  */
alter table aid_user
    add password varchar(100) null;


update aid_user set  `password` = '$2a$10$AlC6PZoBjy24FPerVPKvOuzLoEV5QmvRq2A7a35ri9O/CEssS0dzK';



/*  26/10  */
create table aid_comentario
(
    id smallint not null AUTO_INCREMENT
        primary key,
    contenido varchar(255) null,
    user_id smallint not null,
    ticket_id smallint not null,
    comment_date datetime not null,
    constraint fk_comentario_usuario_
        foreign key (user_id) references aid_user (id),
    constraint fk_comentarios_ticket_
        foreign key (ticket_id) references aid_ticket (id)
);

