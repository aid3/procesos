var request = require("request");
var services = require("../src/conf/services")

describe("Server", () => {
    var server;
    beforeAll(() => {
        server = require("../src/index")
    });
    afterAll(() => {
        server.close();
    });
    describe("GET /", () => {
        var data = {};
        beforeAll((done) => {
            request.get("http://localhost:3000/", (error, response, body) => {
                data.status = response.statusCode;
                data.body = body;
                done();
            });
        });
        it("Status 200", () => {
            expect(data.status).toBe(200);
        });
        it("Body", () => {
            expect(data.body).toBe("prueba");
        });
    });
    describe('Get /user', () => {
        var respuesta = {};
        var options = {
            url: 'http://localhost:3000/getnombre',
            headers: {
                'authorization': services.createToken("Douglas")
            }
        };
        beforeAll((done) => {
            request.get(options, (error, response, body) => {
                respuesta.status = response.statusCode;
                respuesta.body = JSON.parse(body);
                done();
            });
        });
        it("Status 200", () => {
            expect(respuesta.status).toBe(200);
        });
        it("Body", () => {
            expect(respuesta.body.user).toBe("Douglas");
        });
    });
});

