const request = require('supertest')

const app = require('../src/index')

/**
 * Test de crear tokens exitoso.
 */
describe("/POST /tokens", () => {
    it('validar token, exitoso.', done => {
        const data = {
            token: "8"
        }
        request(app)
            .post('/tokens')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})


/**
 * Test de crear tokens fallido.
 */
describe("/POST /tokens", () => {
    it('validar token, el token no existe', done => {
        const data = {
            token: "-98"
        }
        request(app)
            .post('/tokens')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect({
                valid: false,
                user: null
            })
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})
