const request = require('supertest')

const app = require('../src/index')

/**
 * Test get todos los tikets
 */
describe("/GET /tickets", () => {
    it('respond con un json que contiene una lista de los tikets', done => {
        request(app)
            .get('/tickets')
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    })
})
/**
 * Test obtener solo un tiket
 */
describe("/GET /tickets/:id", () => {
    it('respond con un json que contiene un tiket', done => {
        request(app)
            .get('/tickets/1')
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})

/**
 * Test de crear usuario
 */
describe("/POST /tickets", () => {
    it('respond con un json que contiene un tiket creado', done => {
        const data = {
            summary: 'RESUMEN PRUENAS TIKET',
            description: 'descripcion del tiket de prueba',
            priority: 'baja',
            state: 'espera',
            reporter_id: 1,
            asignee_id: 1,
            department_id: 1
        }
        request(app)
            .post('/tickets')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})

/**
 * Test update douglas
 */
describe("/PUT /tickets", () => {
    it('respond con un json que contiene un tiket modificado', done => {
        const data = {
            id: 1,
            summary: 'cambio resumen toket 01',
            description: 'descripcion del tiket de 1',
            priority: 'baja',
            state: 'finalizado',
            reporter_id: 1,
            asignee_id: 1,
            department_id: 1
        }
        request(app)
            .put('/tickets')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})


/**
 * Test obtener solo un tiket, fallido, no existe el ticket
 */
describe("/GET /tickets/:id", () => {
    it('get ticket que no existe', done => {
        request(app)
            .get('/tickets/-98')
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect({
                status: false,
                message: 'El ticket no existe.',
                exception : []
            })
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})
