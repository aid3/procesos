const request = require('supertest')

const app = require('../src/index')

/**
 * Test get todos los departamentos
 */
describe("/GET /departments", () => {
    it('respond con un json que contiene una lista de los departamentos', done => {
        request(app)
            .get('/departments')
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    })
})
/**
 * Test obtener solo un departamento
 */
describe("/GET /departments/:id", () => {
    it('respond con un json que contiene un departamento', done => {
        request(app)
            .get('/departments/1')
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})

/**
 * Test de crear departamento
 */
describe("/POST /departments", () => {
    it('respond con un json que contiene un departamento creado', done => {
        const data = {
            name: "Finanzas",
            description: "Departamento de finanzas"
        }
        request(app)
            .post('/departments')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})

/**
 * Test update contabilidad
 */
describe("/PUT /departments", () => {
    it('respond con un json que contiene un departamento modificado', done => {
        const data = {
            id: 1,
            name: "Contabilidad A",
            description: "Departamento de contabilidad"
        }
        request(app)
            .put('/departments')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})
