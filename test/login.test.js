const request = require('supertest')

const app = require('../src/index')

/**
 * Login exitoso.
 */
describe("/POST /login", () => {
    it('login exitoso.', done => {
        const data = {
            email: "erik@usuario.com",
            password: "pass123"
        }
        request(app)
            .post('/login')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})

/**
 * Login fallido, mal email y password.
 */
describe("/POST /login", () => {
    it('login fallido.', done => {
        const data = {
            email: "erikerror@usuario.com",
            password: "pass123"
        }
        request(app)
            .post('/login')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect({
                status: false,
                message: 'El usuario no existe.',
                exception : []
            })
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})
