const request = require('supertest')

const app = require('../src/index')

/**
 * Test get todos los usuarios
 */
describe("/GET /reporters", () => {
    it('respond con un json que contiene una lista de los usuarios que reporto', done => {
        request(app)
            .get('/reporters')
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    })
})

/**
 * Test obtener solo un usuario
 */
describe("/GET /reporters/:id", () => {
    it('respond con un json que contiene un usuario que reporto', done => {
        request(app)
            .get('/reporters/1')
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})

/**
 * Test de crear usuario
 */
describe("/POST /reporters", () => {
    it('response con un json que contiene un usuario que reporto creado', done => {
        const data = {
            name: "Julio",
            lastname: "Peres",
            email: "julio123@gmail.com",
            access_token: "TOKEN06"
        }
        request(app)
            .post('/reporters')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})

/**
 * Test update douglas
 */
describe("/PUT /reporters", () => {
    it('respond con un json que contiene un usuario que reporto modificado', done => {
        const data = {
            id: 1,
            name: "Douglas",
            lastname: "Aguilar",
            email: "douglas123@gmail.com"
        }
        request(app)
            .put('/reporters')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})


/**
 * Test de crear usuario, con errores
 */
describe("/POST /reporters", () => {
    it('crear usuario, falla por problemas con los datos enviados', done => {
        const data = {
            name: "Julio",
            lastname: "Peres",
            email: null
        }
        request(app)
            .post('/reporters')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect({
                status: false,
                message: 'Error al procesar solicitud.',
                exception : []
            })
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})
