const request = require('supertest')

const app = require('../src/index')

/**
 * Test de crear comentario exitoso.
 */
describe("/POST /comment", () => {
    it('respond con un json que contiene un comment creado', done => {
        const data = {
            contenido: "Comentario prueba unitaria.",
            user_id: "1",
            ticket_id: "2"
        }
        request(app)
            .post('/comment')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})

/**
 * Test update comentario exitoso.
 */
describe("/PUT /comment", () => {
    it('respond con un json que contiene un comentario modificado', done => {
        const data = {
            contenido: "Comentario modificado, pruebas unitarias.",
            user_id: 3,
            ticket_id: 2,
            id: 1
        }
        request(app)
            .put('/comment')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})

/**
 * Test de crear comentario fallido.
 */
describe("/POST /comment", () => {
    it('No se puede crear comentario, faltan parametros, el usuario.', done => {
        const data = {
            contenido: "Comentario prueba unitaria.",
            ticket_id: "2"
        }
        request(app)
            .post('/comment')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect({
                status: false,
                message: 'Error al crear el comentario.',
                exception : []
            })
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})
