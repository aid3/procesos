const request = require('supertest')

const app = require('../src/index')

/**
 * Test get todos los usuarios
 */
describe("/GET /users", () => {
    it('response con un json que contiene una lista de los usuarios', done => {
        request(app)
            .get('/users')
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    })
})
/**
 * Test obtener solo un usuario
 */
describe("/GET /users/:id", () => {
    it('response con un json que contiene un usuario', done => {
        request(app)
            .get('/users/1')
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})

/**
 * Test de crear usuario
 */
describe("/POST /users", () => {
    it('respond con un json que contiene un usuario creado', done => {
        const data = {
            name: "EjemploPruebasUnitarias",
            lastname: "EjemploPruebasUnitarias Apellido",
            password: "pass123",
            email: "julio123@gmail.com",
            department_id: 1
        }
        request(app)
            .post('/users')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})

/**
 * Test update douglas
 */
describe("/PUT /users", () => {
    it('response con un json que contiene un usuario modificado', done => {
        const data = {
            id: 1,
            name: "Douglas",
            lastname: "Aguilar",
            email: "douglas123@gmail.com",
            department_id: 1
        }
        request(app)
            .put('/users')
            .send(data)
            .set('Accept', 'aplication/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(err => {
                if (err) return done(err);
                done();
            })
    });
})
